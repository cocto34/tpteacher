const jwt = require('jsonwebtoken');
const NguoiDung = require('../models/nguoi_dung.schema');
const bcrypt = require('bcrypt');

generateToken = (nd, secretKey, tokenLife) => {
    return new Promise((resolve, reject) => {
        const _data = {
            _id: nd._id,
            ho: nd.ho,
            ten: nd.ten,
            email: nd.email,
            avatar: nd.anh_dai_dien,
        }
        jwt.sign(
            {
                data: _data
            },
            secretKey,
            {
                algorithm: 'HS256',
                expiresIn: tokenLife,
            },
            (err, encoded) => {
                if(err){
                    return reject(err);
                }
                resolve(encoded);
            }
        )
    })
}

verifyToken = (token, secretKey) => {
    return new Promise((resolve, reject) => {
        jwt.verify(token, secretKey, (err, decoded) => {
            if(err){
                return reject(err)
            }
            return resolve(decoded)
        })
    })
}


module.exports = {
    
    generateToken,
    verifyToken,
}