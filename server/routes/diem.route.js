const express = require('express')
const router = express.Router()
const DiemController = require('../controllers/diem.controller')
const ChamDiemController = require('../controllers/cham_diem.controller')

// router.get('/all/:c_id', DiemController.getAllByClassroom)

router.get('/:lop_hoc_id/baitap/:ex_id', DiemController.getExMarkById)

router.get('/:lop_hoc_id/baithi/:ex_id', DiemController.getTestMarkById)

router.get('/baitap/:s_id/:e_id', ChamDiemController.getExOfStudent)

router.get('/baithi/:s_id/:t_id', ChamDiemController.getTestOfStudent)

router.post('/mark', DiemController.mark)

router.post('/update', DiemController.updateMark)

//router.delete('/delete', DiemController.removeMark)

module.exports = router