const express = require('express')
const CauHoiController = require('../controllers/cau_hoi.controller')

const router = express.Router()

router.get('/trac-nghiem', CauHoiController.getFullMultiChoiceQuestions)

router.get('/tu-luan', CauHoiController.getFullEssayQuestions)

router.get('/:nguoi_tao_id/trac-nghiem', CauHoiController.getMultiChoiceQuestions)

router.get('/:nguoi_tao_id/tu-luan', CauHoiController.getEssayQuestions)

router.get('/trac-nghiem/:id', CauHoiController.getMultiChoiceById)

router.get('/tu-luan/:id', CauHoiController.getEssayById)

router.post('/trac-nghiem/bytopic', CauHoiController.getMultiChoiceByTopic)

router.post('/tu-luan/bytopic', CauHoiController.getEssayByTopic)

router.post('/trac-nghiem/add', CauHoiController.addChoiceQuestion)

router.post('/tu-luan/add', CauHoiController.addEssayQuestion)

router.post('/trac-nghiem/search', CauHoiController.searchChoiceQuestions)

router.post('/tu-luan/search', CauHoiController.searchEssayQuestions)

router.post('/search', CauHoiController.search)

router.post('/trac-nghiem/search/bytitle', CauHoiController.searchChoiceQuestionsByTitle)

router.post('/tu-luan/search/bytitle', CauHoiController.searchEssayQuestionsByTitle)

router.post('/trac-nghiem/update', CauHoiController.updateChoiceQuestion)

router.post('/tu-luan/update', CauHoiController.updateEssayQuestion)

router.post('/trac-nghiem/delete', CauHoiController.deleteChoiceQuestion)

router.post('/tu-luan/delete', CauHoiController.deleteEssayQuestion)

module.exports = router;