const express = require('express')
const router = express.Router();

const InviteController = require('../controllers/invite.controller')

router.post('/addcode', InviteController.addCode)

module.exports = router