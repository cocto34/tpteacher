const {Router} = require('express')

const router = Router()
const NguoiDungController = require('../controllers/nguoi_dung.controller')
const TeacherController = require('../controllers/teacher.controller')


router.get('/info/:id', NguoiDungController.getInfoById)

router.post('/info/changepassword', NguoiDungController.changePassword)

router.post('/info/changeinforequest', TeacherController.changeInfoRequest)

module.exports = router