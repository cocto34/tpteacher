const {Router} = require('express');

const router = Router();
const UpLoadController = require('../controllers/upload.controller');


router.get('/avatar/:id', UpLoadController.getAvatar)

router.get('/file/:id', UpLoadController.getDocument)

router.post('/avatar/upload', UpLoadController.uploadAvatar)

router.post('/file/upload', UpLoadController.uploadDocument)

module.exports = router;