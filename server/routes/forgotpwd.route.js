const express = require('express')
const router = express.Router();
const FController = require('../controllers/forgotpwd.controller')
router.get('/resetpassword/:code', FController.checkCode)

router.post('/', FController.forgotPassword)

router.post('/changepassword',FController.resetPassword)

router.post('/deletecode', FController.deleteCode)

module.exports = router;