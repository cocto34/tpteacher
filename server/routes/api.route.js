const express = require('express');
const router = express.Router();
const AuthMiddleware = require('../middleware/authentication');
const AuthController = require('../controllers/auth.controller');


initAPI = (app) => {
    router.post('/dang-nhap', AuthController.signIn)
    router.post('/refresh-token', AuthController.refreshToken);

    router.use(AuthMiddleware.requiresLogin);
    
    return app.use('/',router);
}

module.exports = initAPI;