const express = require('express');
const router = express.Router();
const BaiTapController = require('../controllers/bai_tap.controller');

router.get('/:t_id/lists', BaiTapController.getExBelongToTeacher);

router.get('/:id', BaiTapController.getById)

router.get('/lophoc/:c_id', BaiTapController.getExInClassroom);

router.post('/add', BaiTapController.addEx);

router.post('/update', BaiTapController.updateEx)

router.post('/movetotrashbin', BaiTapController.deleteEx)

router.post('/addcomment', BaiTapController.addExComment)

router.post('/removecomment', BaiTapController.removeComment)

router.post('/search', BaiTapController.searchExers);

module.exports = router;