const express = require('express')
const router = express.Router()
const DanhMucController = require('../controllers/danh_muc.controller')

router.get('/list', DanhMucController.getAll)


module.exports = router;