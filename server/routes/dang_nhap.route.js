const express = require('express');
const DangNhap = require('../controllers/dang_nhap.controller');
const { route } = require('./bai_tap.route');
const router = express.Router();

//Route yêu cầu đăng nhập
router.post('/',(req, res)=>{
    DangNhap.signIn(req, res);
});

//Route xác thực người dùng
// router.post('/xac-thuc',(req, res)=>{
//     DangNhap.verify(req, res, ()=>{
//         //Xử lý
        
//     })
// })
module.exports = router;