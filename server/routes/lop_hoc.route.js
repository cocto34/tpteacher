const express = require('express')
const router = express()
const LopHocController = require('../controllers/lop_hoc.controller')

router.get('/:id/sinh-vien/', LopHocController.getAllStudents)

router.get('/:id/bai-tap/',LopHocController.getAllExcers)

router.get('/:id/bai-thi', LopHocController.getAllTests)

router.get('/:id/bai-viet', LopHocController.getAllPosts)

router.get('/:id/', LopHocController.getAllBelongToTeacher)

router.get('/info/:id', LopHocController.getInfoById)

router.post('/bai-tap/add', LopHocController.addExamToClass)

router.post('/bai-thi/add', LopHocController.addTestToClass)

router.post('/add', LopHocController.addClassroom)

router.post('/bai-tap/remove', LopHocController.removeExamByIndex)

router.post('/bai-thi/remove', LopHocController.removeTestByIndex)

router.post('/addstudent', LopHocController.addStudent)

router.post('/update', LopHocController.updateClassroom)

router.post('/archive', LopHocController.archiveClassroom)

router.post('/removestudent', LopHocController.removeStudent)

module.exports = router
