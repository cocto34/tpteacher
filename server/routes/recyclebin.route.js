const {Router} = require('express')

const RecyclebinController = require('../controllers/recyclebin.controller')

const router = Router()

router.get('/bai-thi/:id', RecyclebinController.getAllTests)

router.get('/bai-tap/:id', RecyclebinController.getAllExcers)

router.post('/bai-tap/search', RecyclebinController.searchExcers)

router.post('/bai-thi/search', RecyclebinController.searchTests)

router.post('/bai-thi/restore/', RecyclebinController.restoreTest)

router.post('/bai-tap/restore/', RecyclebinController.restoreExcer)

router.post('/bai-thi/delete/', RecyclebinController.forceDeleteTest)

router.post('/bai-tap/delete/', RecyclebinController.forceDeleteExcer)

module.exports = router