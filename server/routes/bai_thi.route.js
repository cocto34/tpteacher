const express = require('express');
const router = express.Router();
const BaiThiController = require('../controllers/bai_thi.controller')

router.get('/:nguoi_tao_id/list', BaiThiController.getAllBelongToTeacher)

router.get('/lophoc/:lop_hoc_id', BaiThiController.getTestInClassroom)

router.get('/:id', BaiThiController.getById)

router.post('/add', BaiThiController.addTest)

router.post('/update',BaiThiController.updateTest)

router.post('/delete', BaiThiController.deleteTest)

router.post('/search', BaiThiController.search)

router.post('/addcomment', BaiThiController.addTestComment)

router.post('/removecomment', BaiThiController.removeTestComment)



module.exports = router;