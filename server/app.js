/*
    Module: giáo viên
*/
require('dotenv').config()
//Setup
const mongoose = require('mongoose');
const express = require('express');
const parser = require('body-parser');
const multipart = require('connect-multiparty')
const multipartMiddleware = multipart();
//const bcrypt = require('bcrypt');
//const session = require('express-session');
const cors = require('cors')
//Routes
const BaiTapRoute = require('./routes/bai_tap.route')
const BaiThiRoute = require('./routes/bai_thi.route')
const SinhVienRoute = require('./routes/sinh_vien.route')
const DangNhapRoute = require('./routes/dang_nhap.route')
const NguoiDungRoute = require('./routes/nguoi_dung.route')
const LopHocRoute = require('./routes/lop_hoc.route')
const CauHoiRoute = require('./routes/cau_hoi.route')
const DanhMucRoute = require('./routes/danh_muc.route')
const DiemRoute = require('./routes/diem.route')
const LoiMoiRoute = require('./routes/invite.route')
const QuenMKRoute = require('./routes/forgotpwd.route')
const UpLoadRoute = require('./routes/upload.route')
const RecycleRoute = require('./routes/recyclebin.route')
const Service = require('./service/invite.service')
const app = express();
const AuthMiddleware = require('./middleware/authentication');
const { response } = require('./routes/lop_hoc.route');
mongoose.connect(`mongodb+srv://anhthi:${process.env.PASSWORD}@cluster0-kazzw.mongodb.net/${process.env.DB}?retryWrites=true&w=majority`,{useNewUrlParser:true, useUnifiedTopology:true, useFindAndModify: false, useCreateIndex: true});
//const db = mongoose.connection;
app.listen((process.env.PORT || 3001),()=>{
    console.log('Running server at port '+ 3001);
});
app.use(express.static('public'));
app.set('view engine', 'ejs');
app.set('views', './views');

// App Config
app.use(cors());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', ['*'])
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});
app.use(parser.json({ limit: '100mb' }));
app.use(parser.urlencoded({limit: '100mb', extended: true }))

//Các route được phép truy cập không cần xác thực
app.get('/',(req, res)=>{
    res.render('index');
});
app.get('/aa', async (req, res) => {
    const s = await Service.makeCode()
    res.json({mes: s})
})
// APIs
//Đăng nhập trước để lấy token
app.use('/api/dangnhap', DangNhapRoute)
app.use('/api/file/',multipartMiddleware , UpLoadRoute)
app.use('/api/quen-mat-khau', QuenMKRoute)
//Middleware xác thực bảo vệ API
//app.use(AuthMiddleware.requiresLogin)
//List routes
app.use('/api/loimoi', LoiMoiRoute)
app.use('/api/cauhoi', CauHoiRoute)
app.use('/api/danhmuc',DanhMucRoute)
app.use('/api/baitap', BaiTapRoute)
app.use('/api/baithi',BaiThiRoute)
app.use('/api/sinhvien', SinhVienRoute)
app.use('/api/lophoc', LopHocRoute)
app.use('/api/diem', DiemRoute)
app.use('/api/nguoidung', NguoiDungRoute)
app.use('/api/recyclebin', RecycleRoute)

module.exports = app;