const TeacherService = require('../service/teacher.service')
//const { changeAvatar } = require('../service/teacher.service')


changePassword = async (req, res) => {
    const result = await TeacherService.changePassword(req.body.id, req.body.oldPassword, req.body.pwd)
    result ? res.json({message: 'Đổi mật khẩu thành công', success: true}).status(200) : res.json({message: result.message, success: false})
}

changeAvatar = async (req, res) => {
    const result = await TeacherService.changeAvatar(req.body.id, req.body.url)
    result ? res.json({message: 'Đổi ảnh đại diện thành công', success: true}).status(200) : res.json({message: result.message, success: false})
}

changeInfoRequest = async (req, res) => {
    const result = await TeacherService.changeInfoRequest(req.body)
    result&&result._id ? res.json({message: 'Đã gửi yêu cầu', success: true}).status(200) : res.json({message: result.message, success: false})
}


module.exports = {
    changeAvatar,
    changePassword,
    changeInfoRequest,
}