const NguoiDung = require('../models/nguoi_dung.schema')
const NguoiDungService = require('../service/teacher.service')
getInfoById = async (req, res) => {
    const result = await NguoiDung.findById(req.params.id).then(inf => inf).catch(err => err)
    const r = {ho: result.ho, ten: result.ten, gioi_tinh: result.gioi_tinh, anh_dai_dien: result.anh_dai_dien, ngay_sinh: result.ngay_sinh, email: result.email, sdt: result.sdt, mat_khau: result.mat_khau}
    result&&result._id ? res.json(r).status(200) : res.json({success: false}).status(500)
}

changePassword = async (req, res) => {
    const result = await NguoiDungService.changePassword(req.body.id, req.body.oldPassword, req.body.newPassword)
    result&&result._id ? res.json({success: true}).status(200) : res.json({message: result.message, success: false}).status(500)
}


module.exports = {
    getInfoById,
    changePassword,
}