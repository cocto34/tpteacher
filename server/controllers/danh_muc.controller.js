const DanhMucService = require('../service/danh_muc.service')


getAll = async (req, res) => {
    const result = await DanhMucService.getAll();
    const s = [];
    result.map(x => s.push({id: x._id, value: x.tieu_de}));
    return res.json(s).status(200)
}

module.exports = {
    getAll,
}