const InviteService = require('../service/invite.service')


addCode = async (req, res) => {
    const result = await InviteService.addCode(req.body)
    result&&result._id ? res.status(200).json({_id: result._id, code: result.code, success: true}) : res.status(500).json({message: result.message, success: false})
}

deleteCode = async (req, res) => {
    const result = await InviteService.deleteCode(req.body.s_id, req.body.c_id)
    result ? res.status(200).json({success: true}) : res.status(500).json({message: result.message, success: false})
}

module.exports = {addCode, deleteCode,}