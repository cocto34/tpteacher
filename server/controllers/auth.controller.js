const helper = require('../helpers/jwt.helper');



//Thời gian mặc định của token
const tokenLife = process.env.ACCESS_TOKEN_LIFE || "1h"
//
const accessTokenSecret = process.env.ACCESS_TOKEN_SECRET || 'access_token_secret_teacher@@'
//
const refreshTokenLife = process.env.REFRESH_TOKEN_LIFE || '365d'
//
const refreshTokenSecret = process.env.REFRESH_TOKEN_SECRET || 'refresh_token_secret_teacher@@'
/*
@param{*} req
*/
signIn = async (data, res) => {
    try{
       // const _data = req.body;
        // Tạo mã access token
        const accessToken = await helper.generateToken(data, accessTokenSecret, tokenLife)
        // Tạo mã refresh token (thời gian mặc định là 1 năm)
        const refreshToken = await helper.generateToken(data, refreshTokenSecret, refreshTokenLife)
        const result = {
            user_info: {
                id: data._id,
                email: data.email,
		        loai: false,
            },
            token: accessToken,
            success: true,
        }
        return res.status(200).json(result);
    }
    catch(err){
        return res.status(500).json(err);
    }
}

refreshToken = async (req, res) => {
    //const refreshTokenFromClient = req.body.refreshtoken;
}

module.exports = {
    signIn,
    refreshToken,
}