const LopHoc = require('../models/lop_hoc.schema');
const SinhVien = require('../models/sinh_vien.schema')
const LopHocService = require('../service/lop_hoc.service');
const inviteService = require('../service/invite.service')

getAllStudents = async (req, res) => {
    const result = await LopHocService.getAllStudents(req.params.id);
    if(result === null)
        return res.json({message: 'Không có kết quả', success: false})
    res.json(result).status(200);
}
getAllPosts = async (req, res) => {
    const result = await LopHocService.getAllPosts(req.params.id)
    result ? res.json({result}).status(200) : res.json({message: result.message, success: false})

}

getAllExcers = async (req, res) => {
    const result = await LopHocService.getAllExcers(req.params.id)
    result ? res.json(result).status(200) : res.json({message: result.message, success: false})
}

getAllTests = async (req, res) => {
    const result = await LopHocService.getAllTests(req.params.id)
    result ? res.json(result).status(200) : res.json({message: result.message, success: false})
}

getAllBelongToTeacher = async (req, res) => {
    const result = await LopHocService.getAllBelongToTeacher(req.params.id)
    if(result){
        const r = []
        result.map(x => r.push({
            _id: x._id,
            value: x.tieu_de ? x.tieu_de : '',
            status: x.trang_thai,
        }))
        res.json(r).status(200)
    }
    else{
        res.json({message: result.message, success: false}).status(500)
    }
     
}

removeExamByIndex = async (req, res) => {
const result = await LopHocService.removeExamByIndex(req.body.c_id, req.body.index);
result&&result._id ? res.json(result).status(200) : res.json({message: result.message, success: false}).status(500)
}

removeTestByIndex = async (req, res) => {
const result = await LopHocService.removeTestByIndex(req.body.c_id, req.body.index);
result&&result._id ? res.json(result).status(200) : res.json({message: result.message, success: false}).status(500)
}

getInfoById = async (req, res) => {
    const result = await LopHocService.getInfoById(req.params.id)
    result&&result._id ? res.json(result).status(200) : res.json({message: result.message, success:false}).status(500)
}

addExamToClass = async (req, res) => {
    const result = await LopHocService.addExamToClass(req.body.c_id, req.body.ex_id)
    result&&result._id ? res.json(result).status(200) : res.json({message: result.message, success: false});
}

addTestToClass = async (req, res) => {
    const result = await LopHocService.addTestToClass(req.body.c_id, req.body.ex_id)
    result&&result._id ? res.json(result).status(200) : res.json({message: result.message, success: false}).status(500)
}

removeStudent = async (req, res) => {
    const result = await LopHocService.removeStudent(req.body.c_id, req.body.s_id)
    result&& result._id ? res.json(result).status(200) : res.json({message: result.message, success: false});
}

addStudent = async (req, res) => {
    const lop = await LopHoc.findById(req.body.c_id);
    const isValid = await inviteService.checkCode(req.body.code, req.body.email)
    if(isValid && lop){
        const s_id = await SinhVien.findOne({email: req.body.email})
        if(s_id._id){
            lop.ds_sinh_vien.push(s_id._id)
            inviteService.deleteCode(lop._id)
            s_id.ds_lop_hoc.push(lop._id)
        }
        lop.save().then(l => {
            res.status(200).json({success: true})
        }).catch(err => {res.status(500).json({success: false})})
    }
    else{
        res.status(500).json({sucess: false})
    }
}

addClassroom = async (req, res) => {
    const result = await LopHocService.addClassroom(req.body)
    (result && result._id) ? res.json({_id: result._id, success: true}).status(200) : res.json({message: result.message, success: false}).status(500);
}

updateClassroom = async (req, res) => {
    const result = await LopHocService.updateClassroom(req.body)
    (result && result._id) ? res.json({_id: result._id, success: true}).status(200) : res.json({message: result.message, success: false}).status(500);
}

archiveClassroom = async (req, res) => {
    const result = await LopHocService.archiveClassroom(req.body.id)
    (result && result._id) ? res.json({message: 'Lưu trữ thành công '+result._id, success: true}).status(200) : res.json({message: result.message, success: false})
}

module.exports = {
    getAllExcers,
    getAllPosts,
    getAllStudents,
    getAllTests,
    getAllBelongToTeacher,
    addClassroom,
    updateClassroom,
    archiveClassroom,
    addStudent,
    removeStudent,
    getInfoById,
    addExamToClass,
addTestToClass,
	removeExamByIndex,
removeTestByIndex,
}

