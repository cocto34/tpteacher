const CauHoiService = require('../service/cau_hoi.service')
const TuLuan = require('../models/ch_tu_luan.schema');
const TracNghiem = require('../models/ch_trac_nghiem.schema')
const NguoiDung = require('../models/nguoi_dung.schema')
//const DanhMuc = require('../models/danh_muc.schema')
const DanhMucSV = require('../service/danh_muc.service')
//const { addChoiceQuestion } = require('../service/cau_hoi.service')

getFullMultiChoiceQuestions = async (req, res) => {
    TracNghiem.find({trang_thai: true})
    .populate('danh_muc')
    .populate('nguoi_tao_id')
    .then(q => {
        let result = []
        q.map(c => {
            result.push({
            question: c.noi_dung,
            score: c.diem,
            answers: c.lua_chon ? c.lua_chon : [],
            answer: c.dap_an ? c.dap_an.value : '',
            createBy: c.nguoi_tao_id.ho +' '+ c.nguoi_tao_id.ten,
            topic: c.danh_muc ? c.danh_muc.tieu_de : '',
            })
        })
        res.status(200).json({count: q.length, data: result, success: true});
    })
    .catch(err => {
        res.status(500).json({message: err.message, success: false});
    })
}

getFullEssayQuestions = async (req, res) => {
    TuLuan.find({trang_thai: true})
    .populate('danh_muc')
    .populate('nguoi_tao_id')
    .then(q => {
        let result = []
        q.map(x => {
            result.push({
                question: x.tieu_de,
                createdBy: x.nguoi_tao_id.ho + ' ' +x.nguoi_tao_id.ten,
                topic: x.danh_muc ? x.danh_muc.tieu_de : '',
            })
        })
        res.status(200).json({count: q.length, data: result, success: true});
    })
    .catch(err => {
        res.status(500).json({message: err.message, success: false});
    })
}

// Dành cho loại câu hỏi trắc nghiệm
getMultiChoiceQuestions = async (req, res) => {
    const n_id = req.params.nguoi_tao_id;
    const u = await NguoiDung.findById(n_id);
    TracNghiem.find({nguoi_tao_id: n_id, trang_thai: true})
    .populate('danh_muc')
    .then(q => {
        let result = []
        q.map(c => {
            result.push({
            question: c.noi_dung,
            score: c.diem,
            answers: c.lua_chon ? c.lua_chon : [],
            answer: c.dap_an ? c.dap_an.value : '',
            createBy: u.ho +' '+ u.ten,
            topic: c.danh_muc ? c.danh_muc.tieu_de : '',
            })
        })
        res.status(200).json({count: q.length, data: result, success: true});
    })
    .catch(err => {
        res.status(500).json({message: err.message, success: false});
    })
}


//Dành cho loại câu hỏi tự luận
getEssayQuestions = async (req, res) => {
    const n_id = req.params.nguoi_tao_id
    const u = await NguoiDung.findById(n_id)
    TuLuan.find({nguoi_tao_id: n_id, trang_thai: true})
    .populate('danh_muc')
    .then(q => {
        let result = [];
        q.map(c => {
            result.push({
                question: c.noi_dung,
                createBy: u.ho+ ' '+ u.ten,
                topic: c.danh_muc ? c.danh_muc.tieu_de : '',
            })
        })
        res.status(200).json({count: q.length, data: result,success: true})
    })
    .catch(err => {
        res.status(500).json({message: err.message, success: false});
    })
}

getMultiChoiceById = async (req, res) => {
    const result = await CauHoiService.getMultiChoiceById(req.params.id)
    const r = {
        question: result.noi_dung,
        options: result.lua_chon,
        score: result.diem,
        topic: {
            id: result.danh_muc ? result.danh_muc._id : '',
            title: result.danh_muc ? result.danh_muc.tieu_de : ''
        },
        answer: result.dap_an,
        q_type: 1
    }
    result&&result._id ? res.json(r).status(200) : res.json({message: result.message, success: false}).status(500)
}

getEssayById = async (req, res) => {
    const result = await CauHoiService.getEssayById(req.params.id)
    result&&result._id ? res.json(result).status(200) : res.json({message: result.message, success: false}).status(500)
}

getMultiChoiceByTopic = async (req, res) => {
	const result = await CauHoiService.getMultiChoiceByTopic(req.body.u_id, req.body.topic, req.body.title)
	result ? res.json(result).status(200) : res.json({message: result.message, success: false}).status(500)
}

getEssayByTopic = async (req, res) => {
	const result = await CauHoiService.getEssayByTopic(req.body.u_id, req.body.topic, req.body.title)
	result ? res.json(result).status(200) : res.json({message: result.message, success: false}).status(500)
}

addChoiceQuestion = async (req, res) => {
    const result = await CauHoiService.addChoiceQuestion(req.body);
    (result && result._id) ? res.json({_id: result._id, success: true}).status(200) : res.json({message: result.message, success: false}).status(500);
}

addEssayQuestion = async (req, res) => {
    const result = await CauHoiService.addEssayQuestion(req.body);
    (result && result._id) ? res.json({_id: result._id, success: true}).status(200) : res.json({message: result.message, success: false}).status(500);
}

updateChoiceQuestion = async (req, res) => {
    const result = await CauHoiService.updateChoiceQuestion(req.body);
    (result && result._id) ? res.json({_id: result._id, success: true}).status(200) : res.json({message: result.message, success: false}).status(500);
}

updateEssayQuestion = (req, res) => {
    const {id, noi_dung, diem, danh_muc} = req.body;
    TuLuan.findByIdAndUpdate(id,{noi_dung: noi_dung, diem: diem, danh_muc: danh_muc, updatedAt: new Date()},(err, result) => {
        if(err) res.json({success: false}).status(500);
        if(result === null){
            res.json({message:'Không tìm thấy câu hỏi có id '+id +' này', success: false});
        }
        (result && result._id) ? res.json({_id: result._id, success: true}).status(200) : res.json({message: result.message, success: false}).status(500);
    });
}

deleteChoiceQuestion = async (req, res) =>{
    const result = await CauHoiService.deleteChoiceQuestion(req.body.id)
    result ? res.json({success: true}).status(200) : res.json({message: result.message, success:false}).status(500)
}

deleteEssayQuestion = async (req, res) => {
    const result = await CauHoiService.deleteEssayQuestion(req.body.id)
    result ? res.json({success: true}).status(200) : res.json({message: result.message, success:false}).status(500)
}

search = async (req, res) => {
	const {t_id, q_type, title, topic, gridSetting} = req.body
	const pagination = gridSetting
    const skipValue = pagination.PageIndex * pagination.PageSize
    var info = {}
    if(t_id != "" && topic.length > 0){
        info = {noi_dung: new RegExp(title), nguoi_tao_id: t_id, danh_muc: {$in : topic}, trang_thai: true}
    }
    else if(t_id == "" && topic.length >0)
    {
        info = {noi_dung: new RegExp(title), danh_muc: {$in: topic}, trang_thai: true}
    }
    else if(t_id != "" && topic.length == 0){
        info = {noi_dung: new RegExp(title), nguoi_tao_id: t_id, trang_thai: true}
    }
    else{
        var info = {noi_dung: new RegExp(title), trang_thai: true}
    }
	switch(q_type){
		case 0:
		TracNghiem.find(info).skip(skipValue).limit(pagination.PageSize).sort({createdAt: -1})
		.populate('danh_muc')
		.populate('nguoi_tao_id')
		.then(async (bt) => {
            const allResult = await TracNghiem.find(info)
			const result = []
			 bt.map(x => result.push({
            _id: x._id,
            question: x.noi_dung,
            answers: x.lua_chon,
            answer: x.dap_an ? x.dap_an.value : '',
            score: x.diem,
            topic: x.danh_muc ? x.danh_muc.tieu_de : '',
            createdBy: x.nguoi_tao_id ? x.nguoi_tao_id.ho +' '+x.nguoi_tao_id.ten : '',
            createdAt: x.createdAt,
            updatedAt: x.updatedAt,
        }))
        res.json({count: allResult.length, data: result}).status(200)
		})
		.catch(err => {res.json({message: err.message, success: false})})
		break;
		default:
		TuLuan.find(info).skip(skipValue).limit(pagination.PageSize).sort({createdAt: -1})
		.populate('danh_muc')
		.populate('nguoi_tao_id')
		.then(async (bt) => {
            const allResult = await TuLuan.find(info)
            const result = []
            bt.map(x => result.push({
            _id: x._id,
            question: x.noi_dung,
            score: x.diem,
            topic: x.danh_muc ? x.danh_muc.tieu_de : '',
            createdBy: x.nguoi_tao_id ? x.nguoi_tao_id.ho +' '+x.nguoi_tao_id.ten : '',
            createdAt: x.createdAt,
            updatedAt: x.updatedAt,
        }))
        res.json({count: allResult.length, data: result}).status(200)
		})
		.catch(err => {res.json({message: err.message, success: false})})
		break;
	}
}


searchChoiceQuestions = async (req, res) => {
    let filterByTeacher = {_id: req.body.t_id}
    const pagination = req.body.gridSetting;
    const skipValue = pagination.PageIndex * pagination.PageSize
    let id = {nguoi_tao_id: filterByTeacher._id, trang_thai: true}
    TracNghiem.find(id).skip(skipValue).limit(pagination.PageSize).sort({createdAt: -1})
    .populate('danh_muc')
    .populate('nguoi_tao_id')
    .then((bt) => {
        const result = []
        bt.map(x => result.push({
            _id: x._id,
            question: x.noi_dung,
            answers: x.lua_chon,
            answer: x.dap_an ? x.dap_an.value : '',
            score: x.diem,
            topic: x.danh_muc ? x.danh_muc.tieu_de : '',
            createdBy: x.nguoi_tao_id ? x.nguoi_tao_id.ho +' '+x.nguoi_tao_id.ten : '',
            createdAt: x.createdAt,
            updatedAt: x.updatedAt,
        }))
        res.json({count: result.length, data: result}).status(200)
    })
    .catch(err => {res.json({message: err.message, success: false})});
}

searchEssayQuestions = async (req, res) => {
    let filterByTeacher = {_id: req.body.t_id}
    const pagination = req.body.gridSetting
    const skipValue = pagination.PageIndex * pagination.PageSize
    let id = {nguoi_tao_id: filterByTeacher._id, trang_thai: true}
    TuLuan.find(id).skip(skipValue).limit(pagination.PageSize).sort({createdAt: -1})
    .populate('danh_muc')
    .populate('nguoi_tao_id')
    .then(bt => {
        const result = []
        bt.map(x => result.push({
            _id: x._id,
            question: x.noi_dung,
            score: x.diem,
            topic: x.danh_muc ? x.danh_muc.tieu_de : '',
            createdBy: x.nguoi_tao_id ? x.nguoi_tao_id.ho +' '+x.nguoi_tao_id.ten : '',
            createdAt: x.createdAt,
            updatedAt: x.updatedAt,
        }))
        res.json({count: result.length, data: result}).status(200)
    })
    .catch(err => {res.json({message: err.message, success: false})})
}

searchChoiceQuestionsByTitle = async (req, res) => {
	let t_id = req.body.t_id, tit = req.body.title;
    const pagination = req.body.gridSetting;
    const skipValue = pagination.PageIndex * pagination.PageSize
    let info = {noi_dung: new RegExp(tit),nguoi_tao_id: t_id, trang_thai: true };
    TracNghiem.find(info).skip(skipValue).limit(pagination.PageSize).sort({createdAt: -1})
    .populate('danh_muc')
    .populate('nguoi_tao_id')
    .then((bt) => {
        const result = []
        bt.map(x => result.push({
            _id: x._id,
            question: x.noi_dung,
            answers: x.lua_chon,
            answer: x.dap_an ? x.dap_an.value : '',
            score: x.diem,
            topic: x.danh_muc ? x.danh_muc.tieu_de : '',
            createdBy: x.nguoi_tao_id ? x.nguoi_tao_id.ho +' '+x.nguoi_tao_id.ten : '',
            createdAt: x.createdAt,
            updatedAt: x.updatedAt,
        }))
        res.json({count: result.length, data: result}).status(200)
    })
    .catch(err => {res.json({message: err.message, success: false})});
}

searchEssayQuestionsByTitle = async (req, res) => {
    let t_id = req.body.t_id, tit = req.body.title;
    const pagination = req.body.gridSetting
    const skipValue = pagination.PageIndex * pagination.PageSize
    let info = {nguoi_tao_id: t_id, trang_thai: true, noi_dung: new RegExp(tit) };
    TuLuan.find(info).skip(skipValue).limit(pagination.PageSize).sort({createdAt: -1})
    .populate('danh_muc')
    .populate('nguoi_tao_id')
    .then(bt => {
        const result = []
        bt.map(x => result.push({
            _id: x._id,
            question: x.noi_dung,
            score: x.diem,
            topic: x.danh_muc ? x.danh_muc.tieu_de : '',
            createdBy: x.nguoi_tao_id ? x.nguoi_tao_id.ho +' '+x.nguoi_tao_id.ten : '',
            createdAt: x.createdAt,
            updatedAt: x.updatedAt,
        }))
        res.json({count: result.length, data: result}).status(200)
    })
    .catch(err => {res.json({message: err.message, success: false})})
}

module.exports = {
    getFullMultiChoiceQuestions,
    getFullEssayQuestions,
    getMultiChoiceQuestions,
    getEssayQuestions,
    getMultiChoiceById,
    getEssayById,
	getMultiChoiceByTopic,
	getEssayByTopic,
    addChoiceQuestion,
    addEssayQuestion,
    updateChoiceQuestion,
    updateEssayQuestion,
    deleteChoiceQuestion,
    deleteEssayQuestion,
    searchChoiceQuestions,
    searchEssayQuestions,
	searchChoiceQuestionsByTitle,
	searchEssayQuestionsByTitle,
	search,
}