const ChamDiemService = require('../service/cham_diem.service')
const BaiThiSinhVien = require('../models/bai_thi_sinh_vien.schema');
const NopBaiTap = require('../models/nop_bai_tap.schema');
//const { response } = require('../routes/lop_hoc.route')
//const { getExById } = require('../service/cham_diem.service')


// getAllExInClass = async (req, res) => {
//     const result = await ChamDiemService.getAllExInClass(req.params.ex_id, req.params.t_id)
//     result ? res.status(200).json(result) : res.status(500).json({message: result.message, success: false})
// }
// getAllTestInClass = async(req, res) => {
//     const result = await ChamDiemService.getAllTestInClass(req.params.t_id)
//     result ? res.status(200).json(result) : res.status(500).json({message: result.message, success: false})
// }

// getExById = async (req, res) => {
//     const result = await ChamDiemService.getExById(req.params.e_id)
//     result ? res.status(200).json(result) : res.status(500).json({message: result.message, success: false})
// }

// getTestById = async (req, res) => {
//     const result = await ChamDiemService.getTestById(req.params.t_id)
//     result ? res.status(200).json(result) : res.status(500).json({message: result.message, success: false})
// }

getExOfStudent = async (req, res) => {
    const result = await ChamDiemService.getExOfStudent(req.params.s_id, req.params.e_id)
    if(!result){
        res.json({message: 'Không tìm thấy bài nộp này', success: false}).status(500)
    }
    result ? res.status(200).json(result) : res.status(500).json({message: result.message, success: false})
}

getTestOfStudent = async (req, res) => {
    const result = await ChamDiemService.getTestOfStudent(req.params.s_id, req.params.t_id)
    if(!result){
        res.json({message: 'Không tìm thấy bài nộp này', success: false}).status(500)
    }
    result ? res.status(200).json(result) : res.status(500).json({message: result.message, success: false})
}

addMark = async (req, res) => {
    const result = await ChamDiemService.addMark(req.body)
    if(result){
        res.status(200).json({message: 'Chấm điểm thành công rồi nè', result: kq, success: true})
    }   
    else
    {
        res.status(500).json({message: result.message, success: false})
    }

}

module.exports = {
    // getAllExInClass,
    // getAllTestInClass,
    // getExById,
    // getTestById,
    getExOfStudent,
    getTestOfStudent,
    addMark,
}