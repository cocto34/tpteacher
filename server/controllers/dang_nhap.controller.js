'use strict';
require('dotenv').config();
const {MESSAGES} = require('../contants/messages.contant');
const NguoiDung = require('../models/nguoi_dung.schema');
const AuthController = require('./auth.controller');
const TeacherService = require('../service/teacher.service')
//const jwt = require('jsonwebtoken');
signIn = async (req, res)=>{
    NguoiDung.findOne({email: req.body.email, loai: false}, async (err, nguoi_dung)=>{
        if(err) throw err;
        if(!nguoi_dung){
            res.status(401).json({message: MESSAGES.USER_NOT_EXIST, success: false});
        }
        else{
            const isValid = await TeacherService.checkUser(nguoi_dung.mat_khau, req.body.mat_khau)
            if(!isValid){
                res.status(401).json({message: MESSAGES.PASSWORD_WRONG, success: false});
            }
            else{
                const body = {_id: nguoi_dung._id, email: nguoi_dung.email, mat_khau: nguoi_dung.mat_khau}
                return await AuthController.signIn(body, res);
            }
        }
    })
}

// exports.verify = (req, res, next)=>{
//     const access_token = req.headers['authorization'].split(' ')[1];
//     if(access_token != null){
//         jwt.verify(access_token, process.env.ACCESS_TOKEN_SECRET,(err, nd)=>{
//             if(err) throw err;
//             else{
//                 res.json(nd);
//             }
//         });
//     }
//     else{
//         //req.email = '';
//     }
// };

module.exports = {
    signIn,
}