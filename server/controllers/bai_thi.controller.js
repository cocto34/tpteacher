const BaiThiService = require('../service/bai_thi.service')
const BaiThi = require('../models/bai_thi.schema')
const mongoose = require('mongoose')
getAllBelongToTeacher = async (req, res) => {
    const result = await BaiThiService.getAllBelongToTeacher(req.params.nguoi_tao_id)
    result ? res.json(result).status(200) : res.json({message: result.message, success: false})
}

getTestInClassroom = async (req, res) => {
    const result = await BaiThiService.getTestInClassroom(req.params.lop_hoc_id)
    result ? res.json(result).status(200) : res.json({message: result.message, success: false})
}

getById = async (req, res) => {
    const result = await BaiThiService.getById(req.params.id)
    result ? res.json(result).status(200) : res.json({message: result.message, success: false}).status(500)
}

addTest = async (req, res) => {
    const result = await BaiThiService.addTest(req.body)
    result&&result._id ? res.json({_id: result._id, success: true}).status(200) : res.json({message: result.message, success: false}).status(500);
}

addTestComment = async (req, res) => {
    const cmt = {
        id: mongoose.Types.ObjectId(),
        avatar: req.body.avatar,
        name: req.body.name,
        value: req.body.value,
        createAt: new Date(),
    }
    const result = BaiThiService.addTestComment(req.body.id, cmt)
    result ? res.json({message: 'Đã thêm bình luận thành công', success: true}).status(200) : res.json({message: result.message, success: false})
}

updateTest = async (req, res) => {
    const result = await BaiThiService.updateTest(req.body)
    result ? res.json({message: 'Cập nhật thành công', success: true}).status(200) : res.json({message: result.message, success: false}).status(500);
}

deleteTest = async(req, res) => {
    const result = await BaiThiService.deleteTest(req.body.id)
    result ? res.json({message: 'Đã xóa thành công', success: true}).status(200) : res.json({message: result.message, success: false})
}

removeTestComment = async (req, res) => {
    const result = await BaiThiService.removeTestComment(req.body.id, req.body.c_id)
    result ? res.json({message: 'Đã xóa thành công', success: true}).status(200) : res.json({message: result.message, success: false})
}

search = async (req, res) => {
	let {t_id, title, gridSetting} = req.body
    const pagination = gridSetting
    const skipValue = pagination.PageIndex * pagination.PageSize
    let info = {tieu_de: new RegExp(title),nguoi_tao_id: t_id, trang_thai: true }
	BaiThi.find(info).skip(skipValue).limit(pagination.PageSize).sort({createdAt: -1})
	.populate('nguoi_tao_id')
	.populate('lop_hoc_id')
	.then(async (bt) => {
        const allResult = await BaiThi.find(info) 
		const result = []
		bt.map(x => result.push({
			_id: x._id,
			title: x.tieu_de,
			createdBy: x.nguoi_tao_id ? x.nguoi_tao_id.ho+' '+x.nguoi_tao_id.ten : '',
			classroom: x.lop_hoc_id ? x.lop_hoc_id.tieu_de : '',
			date: x.ngay_thi,
			time: x.thoi_gian_thi,
		}))
		res.json({count: allResult.length, data: result}).status(200)
	})
	.catch(err => {res.json({message: err.message, success: false})})
	
}

module.exports = {
    getAllBelongToTeacher,
    getTestInClassroom,
    getById,
    addTest,
    updateTest,
    deleteTest,
    addTestComment,
    removeTestComment,
	search,
}