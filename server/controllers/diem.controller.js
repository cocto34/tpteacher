const DiemService = require('../service/diem.service')


getAllByClassroom = async (req, res) =>{
    const result = await DiemService.getAllByClassroom(req.params.c_id)
    result ? res.status(200).json(result) : res.status(500).json({message: result.message, success: false})
}
// getAllExByClassroom = async () =>{
//     const result = DiemService.getAllExByClassroom(req.params.c_id)
//     result ? res.status(200).json(result) : res.status(500).json({message: result.message, success: false})
// }
getExMarkById = async (req, res) => {
    const result = await DiemService.getExMarkById(req.params.ex_id, req.params.lop_hoc_id)
    const r = []
    result.map(x => r.push({
        id: x._id,
        sinh_vien: `${x.sinh_vien_id.ho} ${x.sinh_vien_id.ten}`,
        diem: x.diem,
    }))
    result ? res.status(200).json(r) : res.status(500).json({message: result.message, success: false})
}

getTestMarkById = async (req, res) => {
    const result = await DiemService.getTestMarkById(req.params.ex_id, req.params.lop_hoc_id)
    const r = []
    result.map(x => r.push({
        id: x._id,
        sinh_vien: `${x.sinh_vien_id.ho} ${x.sinh_vien_id.ten}`,
        diem: x.diem,
    }))
    result ? res.status(200).json(r) : res.status(500).json({message: result.message, success: false})
}

mark = async (req, res) => {
    const result = await DiemService.mark(req.body)
//   console.log(req.body);
    result&&result._id ? res.status(200).json({message: 'Chấm điểm thành công',result: result, success: true}) : res.status(500).json({message: result.message, success: false})
}

updateMark = async (req, res) => {
    const result = await DiemService.updateMark(req.body)
    result&&result._id ? res.status(200).json({message: 'Cập nhật điểm thành công', success: true}) : res.status(500).json({message: result.message, success: false})
}


removeMark = async ( req, res) => {
    const result = await DiemService.removeMark(req.body)
    result ? res.status(200).json({message: 'Xóa điểm thành công', success: true}) : res.status(500).json({message: result.message, success: false})
}

module.exports = {
    getAllByClassroom,
    //getAllExByClassroom,
    getExMarkById,
    getTestMarkById,
    mark,
    updateMark,
    removeMark,
}