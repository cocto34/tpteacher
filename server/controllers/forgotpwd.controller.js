const ForgotPwdService = require('../service/forgotpwd.service')

resetPassword = async (req, res) => {
    const result = await ForgotPwdService.changePassword(req.body.email, req.body.pwd)
    result ? res.json({message: 'Đổi mật khẩu thành công', success: true}).status(200) : res.json({message: result.message, success: false})
}

forgotPassword = async (req, res) => {
    const result = await ForgotPwdService.sendMail(req.body.email)
    result&&!result.message ? res.json({success: true}).status(200) : res.json({success: false}).status(500)
}

checkCode = async (req, res) => {
    const result = await ForgotPwdService.checkCode(req.params.code)
    result ? res.json({email: result.email, success: true}).status(200) : res.json({success: false}).status(500)  
}

deleteCode = async (req, res) => {
    const result = await ForgotPwdService.deleteCode(req.body.code)
    result ? res.json({message: 'Xóa thành công', success: true}).status(200) : res.json({message: result.message, success: false}).status(500)
}

module.exports = {
    checkCode,
    forgotPassword,
    resetPassword,
    deleteCode,
}