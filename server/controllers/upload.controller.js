const fs = require('fs')
const {google} = require('googleapis');
const credentials = require('../credentials.json')
const TOKEN_PATH = require('../token.json')

const NguoiDung = require('../models/nguoi_dung.schema');
const { rejects } = require('assert');
const {client_secret, client_id, redirect_uris} = credentials.web;
const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);


authorize = async () => {
    return await oAuth2Client.refreshToken(TOKEN_PATH.refresh_token).then(res => res).catch(err => console.log(err));
}

getAvatar = async (req, res) => {
    const fileId = req.params.id
    // res.header('Content-Type', 'image/png')
    const auth = await authorize();
    const token = {
        access_token: auth.tokens.access_token,
        refresh_token: auth.tokens.refresh_token,
        scope: auth.tokens.scope,
        token_type: auth.tokens.token_type,
        expiry_date: auth.tokens.expiry_date,
    }
    oAuth2Client.setCredentials(token);
    //listFiles(oAuth2Client, res);
    getMedia(fileId, oAuth2Client, res);
}

uploadAvatar = async (req, res) => {
    //console.log(req.files);
    const auth = await authorize();
    const token = {
        access_token: auth.tokens.access_token,
        refresh_token: auth.tokens.refresh_token,
        scope: auth.tokens.scope,
        token_type: auth.tokens.token_type,
        expiry_date: auth.tokens.expiry_date,
    }
    oAuth2Client.setCredentials(token);
    uploadMedia(oAuth2Client, req, res)
}

uploadDocument = async (req, res) => {
  const auth = await authorize()
  const token = {
    access_token: auth.tokens.access_token,
        refresh_token: auth.tokens.refresh_token,
        scope: auth.tokens.scope,
        token_type: auth.tokens.token_type,
        expiry_date: auth.tokens.expiry_date,
  }
  oAuth2Client.setCredentials(token)
  uploadFile(oAuth2Client, req, res)
}

uploadMedia = (auth, req, res) => {
    //console.log(req.body);
    const folderId = '1WJ5dhf36H5C5oZ8eb2jQp6zRhRP47DbO';
    var fileMetadata = {
      'name': `${req.files.file.name}`,
      'parents': [folderId],
    }
    var media = {
      mimeType: req.files.file.type,
      body: fs.createReadStream(req.files.file.path),
    }
    const drive = google.drive({version: 'v3', auth})
  
    drive.files.create({
        resource: fileMetadata,
        media: media,
        fields: 'id,name'
      }, async (err, file)  => {
          if (err) {
            // Handle error
            return res.json({message: err}).status(500)
          } else {
              await NguoiDung.findByIdAndUpdate(req.body.id, {anh_dai_dien: file.data.id}).then(res=>res).catch(err => err)
            return res.json({id: file.data.id}).status(200)
          }
        });  
}

uploadFile = (auth, req, res) => {
  const folderId = '1GWQOib_qfMm-5JqgVBsFJU53KhwCTgpV'
  var fileMetadata = {
    'name': `${req.files.file.name}`,
    'parents': [folderId],
  }
  var media = {
    mimeType: req.files.file.type,
    body: fs.createReadStream(req.files.file.path),
  }
  const drive = google.drive({version: 'v3', auth})

  drive.files.create({
      resource: fileMetadata,
      media: media,
      fields: 'id,name'
    }, async (err, file)  => {
        if (err) {
          // Handle error
          return res.json({message: err, success: false}).status(500)
        } else {
          drive.permissions.create({
            fileId: file.data.id,   
            requestBody: {
              role: 'reader',
              type: 'anyone',
            }       
          }, function(err , per) {
            if(err) return res.json({message: err, success: false}).status(500)
          })
           return res.json({data: file.data, success: true}).status(200) 
        }
      }); 
}


getMedia = (fileId, auth, res) => {
    //res.setHeader('Content-Type', 'image/jpeg')
    const drive = google.drive({version: 'v3', auth})
    drive.files.get(
      {
        fileId: fileId,
        //fields: '*',
        alt: "media",
        //supportsAllDrives: true
      },
      {responseType: 'stream'},
      function(err, {data}) {
        if (err) {
          return rejects("The API returned an error: " + err);
        }
        let buf = [];
        data.on("data", function(e) {
          buf.push(e);
        });
        data.on("end", function() {
          const buffer = Buffer.concat(buf);
          res.end(buffer);
        });
      }
    );
  }

  getDocument = async (req, res) => {
    const auth = await authorize()
  const token = {
        access_token: auth.tokens.access_token,
        refresh_token: auth.tokens.refresh_token,
        scope: auth.tokens.scope,
        token_type: auth.tokens.token_type,
        expiry_date: auth.tokens.expiry_date,
  }
  oAuth2Client.setCredentials(token)
  getFile(req.params.id, oAuth2Client, res)
  }

  getFile = (fileId, auth, res) => {
    const drive = google.drive({version: 'v3', auth})
    drive.files.get({
      fileId: fileId,
      fields: '*',
    }, function (err , file){
      if(err) console.log('API returned: '+err);
      res.redirect(file.data.webContentLink);
    })
  }


  module.exports = {
      getAvatar,
      getDocument,
      uploadAvatar,
      uploadDocument
  }