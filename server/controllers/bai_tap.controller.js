//const BaiTap = require('../models/bai_tap.schema');
const BaiTapService = require('../service/bai_tap.service');
const mongoose = require('mongoose');
const BaiTap = require('../models/bai_tap.schema')

getAll = async (req, res) => {
    const result = await BaiTapService.getAll();
    return res.status(200).json(result);
}

getById = async (req, res) => {
    if(!req.params.id) return res.status(402).json({message: 'Không có id này', success: false});
    const result = await BaiTapService.getById(req.params.id);
    result&&result._id ? res.status(200).json(result) : res.status(500).json({message: result.message, success: false});
}

getExBelongToTeacher = async (req, res) => {
    const result = await BaiTapService.getExBelongToTeacher(req.params.t_id);
    result ? res.status(200).json(result) : res.status(500).json({message: result.message, success: false});
}

getExInClassroom = async (req, res) => {
	const result = await BaiTapService.getExInClassroom(req.params.c_id);
	result ? res.status(200).json(result) : res.status(500).json({message: result.message, success: false});
}

addEx = async (req, res) => {
    const result = await BaiTapService.addEx(req.body);
    (result && result._id) ? res.json({_id: result._id, success: true}).status(200) : res.json({message: result.message, success: false}).status(500);
}

addExComment = async (req, res) => {
    const cmt = {
        id: mongoose.Types.ObjectId(),
        avatar: req.body.avatar,
        name: req.body.name,
        value: req.body.value,
        createAt: new Date(),
    }
    const result = await BaiTapService.addExComment(req.id, cmt)
    result ? res.json({message: 'Thêm bình luận thành công', success: true}).status(200) : res.json({message: result.message, success: false})
    
}

updateEx = async (req, res) => {
    const result = await BaiTapService.updateEx(req.body);
    (result && result._id) ? res.json({_id: result._id, success: true}).status(200) : res.json({message: result.message, success: false}).status(500);
}

deleteEx = async (req, res) => {
    const result = await BaiTapService.deleteEx(req.body.id);
    return res.status(200).json(result);
}

removeComment = async (req, res) => {
    const result = await BaiTapService.removeComment(req.body.e_id, req.body.c_id)
    result ? res.status(200).json({message: 'Xóa thành công', success: true}) : res.status(500).json({message: result.message, success: false})
}

searchExers = async (req, res) => {
    let {t_id, title, gridSetting} = req.body
    const pagination = gridSetting
    const skipValue = pagination.PageIndex * pagination.PageSize
    let id = {tieu_de: new RegExp(title), nguoi_tao_id: t_id, trang_thai: true}
    BaiTap.find(id).skip(skipValue).limit(pagination.PageSize).sort({createdAt: -1})
	.populate('nguoi_tao_id')
	.populate('lop_hoc_id')
    .then(async (bt) => {
        const allResult = await BaiTap.find(id) 
		const result = []
		bt.map(x => result.push({
			title: x.tieu_de,
			content: x.noi_dung,
			createdBy: x.nguoi_tao_id ? x.nguoi_tao_id.ho + ' '+x.nguoi_tao_id.ten : '',
			classroom: x.lop_hoc_id ? x.lop_hoc_id.tieu_de : '',
			deadline: x.han_nop_bai
		}))
        res.json({count: allResult.length, data: result} ).status(200);
    })
    .catch(err => {res.json({message: err.message, success: false})});
}

module.exports = {
    getAll,
    getById,
    addEx,
    updateEx,
    deleteEx,
    getExBelongToTeacher,
    removeComment,
    addExComment,
    searchExers,
    getExInClassroom,
}