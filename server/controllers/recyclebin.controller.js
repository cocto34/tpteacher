const RecycleService = require('../service/recyclebin.service')

const BaiTap = require('../models/bai_tap.schema')
const BaiThi = require('../models/bai_thi.schema')

// getAllTests = async (req, res) => {
//     const result = RecycleService.getAllTests(req.params.id)
//     result ? res.status(200).json(result) : res.status(500).json({message: result.message, success: false})
// }

// getAllExcers = async (req, res) => {
//     const result = RecycleService.getAllExcers(req.params.id)
//     result ? res.status(200).json(result) : res.status(500).json({message: result.message, success: false})
// }

searchExcers = async (req, res) => {
    const {t_id, gridSetting, title} = req.body;
    const pagination = gridSetting
    const skipValue = pagination.PageIndex * pagination.PageSize
    let info = {tieu_de: new RegExp(title), nguoi_tao_id: t_id, trang_thai: false}
    BaiTap.find(info).skip(skipValue).limit(pagination.PageSize).sort({createdAt: -1})
    .populate('nguoi_tao_id')
    .populate('lop_hoc_id')
    .then(t => {
        const result = []
        t.map(x=> result.push({
            id: x._id,
            title: x.tieu_de,
            createdBy: x.nguoi_tao_id ? `${x.nguoi_tao_id.ho} ${x.nguoi_tao_id.ten}` : '',
            classroom: x.lop_hoc_id ? `${x.lop_hoc_id.tieu_de}` : '',
        }))
        return res.json({count: result.length, data: result}).status(200) 
    })
    .catch(err => {return res.json({message: err.message, success: false})})
}

searchTests = async (req, res) => {
    const {t_id, gridSetting, title} = req.body;
    const pagination = gridSetting
    const skipValue = pagination.PageIndex * pagination.PageSize
    let info = {tieu_de: new RegExp(title), nguoi_tao_id: t_id, trang_thai: false}
    BaiThi.find(info).skip(skipValue).limit(pagination.PageSize).sort({createdAt: -1})
    .populate('nguoi_tao_id')
    .populate('lop_hoc_id')
    .then(t => {
        const result = []
        t.map(x=> result.push({
            id: x._id,
            title: x.tieu_de,
            createdBy: x.nguoi_tao_id ? `${x.nguoi_tao_id.ho} ${x.nguoi_tao_id.ten}` : '',
            classroom: x.lop_hoc_id ? `${x.lop_hoc_id.tieu_de}` : '',
        }))
        return res.json({count: result.length, data: result}).status(200) 
    })
    .catch(err => {return res.json({message: err.message, success: false})})
}

forceDeleteExcer = async (req, res) => {
    const result = await RecycleService.forceDeleteExcer(req.body.id)
    result ? res.status(200).json({message: 'Xóa thành công', success: true}) : res.status(500).json({message: result.message, success: false})
}

forceDeleteTest = async (req, res) => {
    const result = await RecycleService.forceDeleteTest(req.body.id)
    result ? res.status(200).json({message: 'Xóa thành công', success: true}) : res.status(500).json({message: result.message, success: false})
}

restoreTest = async (req, res) => {
    const result = await RecycleService.restoreTest(req.body.id)
    result&&result._id ? res.status(200).json({message: 'Phục hồi thành công', success: true}) : res.status(500).json({message: result.message, success: false})
}

restoreExcer = async (req, res) => {
    const result = await RecycleService.restoreExcer(req.body.id)
    result&&result._id ? res.status(200).json({message: 'Phục hồi thành công', success: true}) : res.status(500).json({message: result.message, success: false})
}
module.exports = {
    getAllExcers,
    getAllTests,
    searchExcers,
    searchTests,
    forceDeleteExcer,
    forceDeleteTest,
    restoreExcer,
    restoreTest,
}