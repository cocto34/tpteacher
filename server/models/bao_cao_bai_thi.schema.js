const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const BaoCaoSchema = new Schema({
    id: Number,
    bai_thi_id: Number,
    noi_dung: String,
    nguoi_bao_cao: Number,
});

module.exports = mongoose.model('BaoCao',BaoCaoSchema, 'bao_cao_bai_thi');