const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const BinhLuanSchema = new Schema({
    id: Number,
    thong_bao_id: Number,
    noi_dung: String,
    nguoi_gui: Number,
    loai_nguoi_dung: Number,
});

module.exports = mongoose.model('BinhLuan', BinhLuanSchema, 'binh_luan');