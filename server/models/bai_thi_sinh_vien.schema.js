const mongoose = require('mongoose')

const Schema = mongoose.Schema
const chiTietBaiThi = {
    cau_hoi_id: {type: Schema.Types.ObjectId, refPath: 'bai_thi_sinh_vien.loai'},
    dap_an: String,
    loai: {type: String, enum:['TracNghiem', 'TuLuan'], default: 'TracNghiem'},
}
const BaiThiSinhVienSchema = new Schema({
    bai_thi_id: {type: Schema.Types.ObjectId, ref: 'BaiThi'},
    bai_thi_sinh_vien: [chiTietBaiThi],
    sinh_vien_id: {type: Schema.Types.ObjectId, ref: 'SinhVien'},
    da_cham_diem: Boolean,
}, {timestamps: true})

module.exports = mongoose.model('BaiThiSinhVien', BaiThiSinhVienSchema, 'bai_thi_sinh_vien')