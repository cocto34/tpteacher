const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const BaiThiGocSchema = new Schema({
    id: Number,
    ds_cau_hoi: [{type: Schema.Types.ObjectId, ref: 'CauHoi'}],
});

module.exports = mongoose.model('BaiThiGoc', BaiThiGocSchema, 'bai_thi_goc');