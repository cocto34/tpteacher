const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const BaiThiLKSchema = new Schema({
    id: Number,
    bai_thi_lk_id: Number,
});

module.exports = mongoose.model('BaiThiLK', BaiThiLKSchema, 'bai_thi_lien_ket');