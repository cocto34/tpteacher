/*
    Author: Nguyen Anh Thi
    LichSuSchema: Mô hình hóa cho đối tượng LichSu
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const LichSuShema = new Schema({
    id: String,
    noi_dung: String,
    nguoi_tao: Number,
    ngay_tao: Date,
    lop_hoc_id: Number,
    });

module.exports = mongoose.model('LichSu',KetQuaSchema, 'lich_su');