const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const InviteSchema = new Schema({
    code: String,
    email: String,
    lop_hoc_id: {type: Schema.Types.ObjectId, ref: 'LopHoc'},
    nguoi_moi_id: {type: Schema.Types.ObjectId, ref: 'NguoiDung'},
    kich_hoat: {type: Boolean, default: false}
}, {timestamps: true})

module.exports = mongoose.model('Invite', InviteSchema, 'invite')