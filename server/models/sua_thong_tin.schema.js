const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const thongTin = new Schema({ho: String,ten: String, ngay_sinh: Date, sdt: String})
const SuaThongTinSchema = new Schema({
    nguoi_dung_id: {type:Schema.Types.ObjectId, ref: 'NguoiDung'},
    ly_do: String,
    thong_tin_sua: thongTin,
    loai: {type:String, default: "GiaoVien"},
    trang_thai: {type: Boolean, default: false}
}, {timestamps: true})

module.exports = mongoose.model('SuaThongTin', SuaThongTinSchema, 'sua_thong_tin')
