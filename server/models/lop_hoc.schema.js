/*
    Author: Nguyen Anh Thi
    LopHocSchema: Mô hình hóa cho đối tượng LopHoc
*/
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const baiThi = new Schema({})

const LopHocSchema = new Schema({
    id: Number,
    tieu_de: {type: String, required: [true, 'Bắt buộc phải nhập tên lớp']},
    nguoi_tao_id: {type: Schema.Types.ObjectId, required: true},
    ds_bai_thi: [{type: Schema.Types.ObjectId, ref: 'BaiThi'}],
    ds_bai_tap: [{type: Schema.Types.ObjectId, ref: 'BaiTap'}],
    ds_sinh_vien: [{type: Schema.Types.ObjectId, ref: 'SinhVien'}],
    trang_thai: {type: Boolean, default: true}
} , {timestamps: true});

module.exports = mongoose.model('LopHoc',LopHocSchema, 'lop_hoc');

