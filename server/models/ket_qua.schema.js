const mongoose = require('mongoose');
//const { ObjectID } = require('mongodb');
const Schema = mongoose.Schema;
const KetQuaSchema = new Schema({
    id: Number,
    nguoi_dung_id: Number,
    lop_hoc_id: Number,
    bai_thi_id: Number,
    cau_hoi: Array,
    ngay_cong_bo: Date
});

module.exports = mongoose.model('KetQua',KetQuaSchema, 'ket_qua_thi');