const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ChuDeSchema = new Schema({
    id: Number,
    tieu_de: String,
    mo_ta: String,
    nguoi_tao: Number
});

module.exports = mongoose.model('ChuDe', ChuDeSchema, 'chu_de');