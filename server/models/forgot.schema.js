const mongoose = require('mongoose')

const Schema = mongoose.Schema

setDateTime = () => {
    return Date.now() + 3600*1000;   
}

const QuenMatKhauSchema = new Schema({
    code: String,
    expire: {type: Number, default: setDateTime()},
    email: String,
})

module.exports = mongoose.model('QuenMatKhau', QuenMatKhauSchema, 'quen_mat_khau')