const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tapTin = {type: String, }
const NoiBaiTapSchema = new Schema({
    bai_tap_id: {type: Schema.Types.ObjectId, ref: 'BaiTap'},
    sinh_vien_id: {type: Schema.Types.ObjectId, ref: 'SinhVien'},
    lop_hoc_id: {type: Schema.Types.ObjectId, ref: 'LopHoc'},
    tap_tin: [tapTin],
    thoi_gian_nop: Date,
    da_cham_diem: {type: Boolean},
}, {timestamps: true});

module.exports = mongoose.model('NopBaiTap', NoiBaiTapSchema, 'nop_bai_tap');