const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const TapTinSchema = new Schema({
    id: Number,
    noi_dung: String,
    duong_dan: String,
});

module.exports = mongoose.model('TapTin', TapTinSchema, 'cau_hoi_tap_tin');