/*
    Author: Nguyen Anh Thi
    DanhMucSchema: Mô hình hóa cho đối tượng DanhMuc
*/
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const DanhMucSchema = new Schema({
    tieu_de: String,
    mo_ta: String,
    nguoi_tao_id: {type: Schema.Types.ObjectId, ref: 'NguoiDung'},
    trang_thai: Boolean,
});

module.exports = mongoose.model('DanhMuc', DanhMucSchema, 'danh_muc');