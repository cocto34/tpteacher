/*
    Author: Nguyen Anh Thi
    SinhVienSchema: Mô hình hóa cho đối tượng SinhVien
*/
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const SinhVienSchema = new Schema({
    ma_sv: {type:String, required: true},
    ho: String,
    ten: String,
    anh_dai_dien: String,
    email: {type:String, required: [true, 'Phải nhập email'], unique: true},
    ngay_sinh: Date,
    mat_khau: {type: String, required: [true, 'Phải nhập mật khẩu']},
    nguoi_tao: Number,
    ds_lop_hoc: [{type: Schema.Types.ObjectId, ref: 'LopHoc'}]
});

module.exports = mongoose.model('SinhVien', SinhVienSchema, 'sinh_vien');