/*
    Author: Nguyen Anh Thi
    NguoiDungSchema: Mô hình hóa cho đối tượng NguoiDung
*/
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const NguoiDungSchema = new Schema({
    id: Number,
    ho: String,
    ten: String,
    anh_dai_dien: String,
    email: {type: String, trim: true, unique: true, required: [true, 'Phải nhập email']},
    ngay_sinh: Date,
    gioi_tinh: Boolean,
    sdt: String,
    mat_khau: {type: String, required: [true, 'Phải nhập mật khẩu']},
    loai: Boolean,
});
NguoiDungSchema.methods.comparePassword = (mat_khau)=>{
    return this.mat_khau == mat_khau;
};
module.exports = mongoose.model('NguoiDung', NguoiDungSchema, 'nguoi_dung');

