const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ThongBaoSchema = new Schema({
    id: Number,
    tieu_de: String,
    noi_dung: String,
    ds_binh_luan: [{type: Schema.Types.ObjectId, ref: 'BinhLuan'}],
});

module.exports = mongoose.model('ThongBao', ThongBaoSchema, 'thong_bao');
