var p = document.getElementsByClassName('part-content');
var h = document.getElementsByTagName('h3');
let count = false;
var c = null;
var setSize = (w,h,element)=>{element.style.width = w; element.style.height = h};
var getSize = (element) => [element.style.width, element.style.height];
document.addEventListener('click',(ev)=>{
    if(ev.target.nodeName == 'H3'){
        c = ev.target.nextElementSibling;
        if(c.style.maxHeight == '60px'){
            c.style.maxHeight = '0';
        }
        else{
            //setSize('auto','auto',c);
            c.style.maxHeight = '60px';
        }
    }
    if(ev.target.innerText == 'Collapse all'){
        for (let index = 0; index < p.length; index++) {
            const element = p[index];
            element.style.maxHeight = '0';
        }
    }
    if(ev.target.innerText == 'Expand all'){
        for (let index = 0; index < p.length; index++) {
            const element = p[index];
            element.style.maxHeight = '60px';
        }
    }
});