
const helper = require('../helpers/jwt.helper');
const e = require('express');

// Giá trị access token secret lấy từ biến môi trường hoặc được mặc định
const accessTokenSecret = process.env.ACCESS_TOKEN_SECRET || "access_token_secret_teacher@@";

requiresLogin = async (req, res, next)=>{
    // Lấy token từ client, token được truyền bằng header
    const tokenFromClient = req.headers['authorization'];
    //console.log(req.headers['authorization']);
    //console.log(tokenFromClient);
    if(typeof(tokenFromClient) !== undefined){
        try{
            // Xác thực mã token xem có hợp lệ không
            const verified = await helper.verifyToken(tokenFromClient.split(' ')[1], accessTokenSecret)
            // Lưu token xác thực vào request để sử dụng cho các xử lý sau
            req.verified = verified;
            next();
        }
        catch(err)
        {
            return res.status(401).json({
                message: 'Không được phép truy cập',
                success: false,
            })
        }
    }
    else{
        return res.status(403).json({
            message: 'Vui lòng cung cấp mã xác thực.',
            success: false,
        })
    }
}


// auth = async(req, res, next) =>{
//     try{
//         const token = req.header('Authorization').replace('Bearer ','')
//         const data = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
//         if(req.method === 'get'){
//             const {email} = req.params;
//             if(id !== data.email){
//                 throw new Error();
//             }
//         }
//         await NguoiDung.findOne({_id: data.id, password: data.mat_khau}, (err, nd)=>{
//             if(err) return next(err);
//             req.nd = nd;
//             req.token = token;
//             return next();
//         })

//     }
//     catch(error){
//         res.status(401).send(error.message);
//     }
// }

module.exports = {
    requiresLogin,
    //decodeToken
}