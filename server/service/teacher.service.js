const NguoiDung = require('../models/nguoi_dung.schema')
const SuaThongTin = require('../models/sua_thong_tin.schema')
const bcrypt = require('bcrypt')

changePassword = async (id, oldPwd, pwd) => {
    const user = await NguoiDung.findById(id)
    const check = await checkUser(user.mat_khau, oldPwd)
    if(check){
        const hash = bcrypt.hashSync(pwd, 10)
        NguoiDung.findByIdAndUpdate(id, {mat_khau: hash})
        user.mat_khau = hash
        return await user.save().then(u => u).catch(err => err)
    }
    return new Error('Mật khẩu hiện tại không đúng');
}

changeInfoRequest = async (body) => {
    const {id, ho, ten, ngay_sinh, gioi_tinh, sdt , ly_do} = body;
    const stt = new SuaThongTin({
        nguoi_dung_id: id,
        ly_do: ly_do,
        thong_tin_sua: {
            ho: ho ? ho :'',
            ten: ten ? ten : '',
            ngay_sinh: ngay_sinh ? ngay_sinh : '',
            gioi_tinh: gioi_tinh ? gioi_tinh : '',
            sdt: sdt ? sdt : '',
        },
        trang_thai: false,
    })
    return await stt.save().then(inf => inf).catch(err => err)
}

// changeAvatar = async (id, url) => {
//     return await NguoiDung.findByIdAndUpdate(id, {anh_dai_dien: url}).then(u => u).catch(err => err)
// }

checkUser = async (u_pwd, pwd) => {
    return await bcrypt.compare(pwd, u_pwd)
}

module.exports = {
    changePassword,
    checkUser,
    changeInfoRequest,
}
