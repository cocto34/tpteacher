const SendMailService = require('./sendmail.service')
const Invite = require('../models/invite.schema')
const NguoiDung = require('../models/nguoi_dung.schema')
const SinhVien = require('../models/sinh_vien.schema')
const LopHoc = require('../models/lop_hoc.schema')
const {InviteHTML} = require('../contants/messages.contant')

initCode = async () => {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < 6; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

addCode = async (body) => {
    const {email, lop_hoc_id, nguoi_moi_id} = body;
    const check = await SinhVien.findOne({email: email})
    if(!check) return new Error('Sinh viên không tồn tại')
    const croom = await LopHoc.findById(lop_hoc_id)
    if(!croom) return new Error('Lớp học không tồn tại')
    if(croom.ds_sinh_vien.indexOf(check._id) >=0) return new Error('Sinh viên đã tồn tại trong lớp học')
    const code = await initCode();
    const inv = new Invite({
        code: code,
        email: email,
        lop_hoc_id: lop_hoc_id,
        nguoi_moi_id: nguoi_moi_id, 
    })
    const c_name = await LopHoc.findById(lop_hoc_id)
    const t_name = await NguoiDung.findById(nguoi_moi_id)
    await SendMailService.sendTo(email, 'MỜI THAM GIA LỚP HỌC', '', InviteHTML(c_name.tieu_de, t_name.ho+' '+t_name.ten, code))
    return await inv.save().then(i => i).catch(err => err)
}

deleteCode = async (lop_hoc_id) => {
    return await Invite.findOneAndUpdate({lop_hoc_id: lop_hoc_id}, {kich_hoat: true})
}

checkCode = async (code, email) => {
    const c = await Invite.findOne({email: email, code: code, kich_hoat: false})
    return c && c._id
}


module.exports = {
    addCode,
    deleteCode,
    checkCode,
}