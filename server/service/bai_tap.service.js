const BaiTap = require('../models/bai_tap.schema')
const mongoose = require('mongoose')

getAll = async () => {
    return await BaiTap.find({trang_thai: true}).then(bt=>bt).catch(err => err)
}

getById = async (id) => {
    return await BaiTap.findById(id).then(bt => bt).catch(err => err)
}

getExBelongToTeacher = async (t_id) => {
    return await BaiTap.find({nguoi_tao_id: t_id, trang_thai: true}).sort({createdAt: -1}).then(bt => bt).catch(err => err);
}

getExInClassroom = async (c_id) => {
return await BaiTap.find({lop_hoc_id: c_id, trang_thai: true}).sort({createdAt: -1}).then(bt => bt).catch(err => err);
}

addEx = async (body) => {
    const {tieu_de, noi_dung,diem, han_nop_bai, 
        nguoi_tao_id, lop_hoc_id, tap_tin} = body
    const bt = new BaiTap({
        _id: mongoose.Types.ObjectId(),
        tieu_de: tieu_de,
        noi_dung: noi_dung,
	    diem: diem,
        nguoi_tao_id: nguoi_tao_id,
        lop_hoc_id: lop_hoc_id,
        han_nop_bai: han_nop_bai,
        tap_tin: tap_tin ? tap_tin : '',
    });
    return await bt.save().then(bt => bt).catch(err => err);
}

addExComment = async (e_id, cmt) => {
    const bt = await BaiTap.findById(e_id)
    bt.ds_binh_luan.push(cmt)
    return await bt.save().then(bt => bt).catch(err => err)
}


updateEx = async (body) => {
    return await BaiTap.findByIdAndUpdate(body.id, {
        tieu_de: body.tieu_de,
        noi_dung: body.noi_dung,
	    diem: body.diem,
        tap_tin: body.tap_tin,
        han_nop_bai: body.han_nop_bai,
    }).then(bt => bt).catch(err => err)
}

deleteEx = async (id) => {
    return await BaiTap.findByIdAndUpdate(id, {trang_thai: false});
}
removeComment = async (e_id, id) =>{
    const bt = await BaiTap.findById(e_id);
    const index = bt.ds_binh_luan.findIndex(x => x.id == id)
    bt.ds_binh_luan.splice(index,1)
    return await bt.save().then(bt => bt).catch(err => err)
}

module.exports = {
    getAll,
    getById,
    addEx,
    updateEx,
    deleteEx,
    getExBelongToTeacher,
    removeComment,
    addExComment,
    getExInClassroom,
}