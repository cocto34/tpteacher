//const SinhVien = require('../models/sinh_vien.schema')
const QuenMatKhau = require('../models/forgot.schema')
const NguoiDung = require('../models/nguoi_dung.schema')
const MailService = require('./sendmail.service')
const bcrypt = require('bcrypt')
const {ResetPasswordHTML} = require('../contants/messages.contant')

makeCode = async () => {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < 50; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

checkCode = async (code) => {
    return await QuenMatKhau.findOne({code: code, expire: {$gt: Date.now()}})
}

sendMail = async (email) => {
    const filter = await NguoiDung.findOne({email: email, loai: false})
    if(filter && filter._id){
        const code = await makeCode()
        const mail = await MailService.sendTo(email, 'QUÊN MẬT KHẨU', '', ResetPasswordHTML(code))
        const fp = new QuenMatKhau({
            code: code,
            email: email,
        })
        return fp.save().then(s => s).catch(err => err)
    }
    return new Error('Email không tồn tại');
}

deleteCode =async (code) => {
    return await QuenMatKhau.findOneAndUpdate({code:code}, {expire: -Date.now()}).then(r => r).catch(err => err)
}


changePassword = async (email, pwd) => {
    pwd = await bcrypt.hash(pwd, 10)
    return await NguoiDung.findOneAndUpdate({email: email} , {mat_khau: pwd} )
    .then(nd => nd)
    .catch(err => err)
}

module.exports = {
    changePassword,
    sendMail,
    checkCode,
    deleteCode,
}
