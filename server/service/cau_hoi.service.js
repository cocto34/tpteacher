const mongoose = require('mongoose')
const TracNghiem = require('../models/ch_trac_nghiem.schema')
const TuLuan = require('../models/ch_tu_luan.schema')
const CauHoi = require('../models/cau_hoi.schema')

// Thêm 1 câu hỏi trắc nghiệm
addChoiceQuestion = async (cau_hoi) => {
    const tn = new TracNghiem();
    tn._id = mongoose.Types.ObjectId();
    tn.noi_dung = cau_hoi.noi_dung ? cau_hoi.noi_dung : '';
    tn.lua_chon = cau_hoi.lua_chon;
    tn.dap_an = cau_hoi.dap_an;
    tn.diem = cau_hoi.diem;
    tn.danh_muc = cau_hoi.danh_muc;
    tn.nguoi_tao_id = cau_hoi.nguoi_tao_id;
    return await tn.save()
    .then(async cau_hoi => {
        const ch = new CauHoi({
            cau_hoi_id: cau_hoi._id,
            loai: 'TracNghiem',
        })
        return await ch.save().then(c => c).catch(err => err)
    })
    .catch(err => err);
}
//Thêm 1 câu hỏi tự luận
addEssayQuestion = async (cau_hoi) =>{
    const tl = new TuLuan();
    tl._id = mongoose.Schema.Types.ObjectId();
    tl.noi_dung = cau_hoi.noi_dung;
    tl.diem = cau_hoi.diem;
    tl.danh_muc = cau_hoi.danh_muc;
    
    tl.nguoi_tao_id = cau_hoi.nguoi_tao_id;
    return await tl.save()
    .then(async cau_hoi => {
        const ch = new CauHoi({
            cau_hoi_id: cau_hoi._id,
            loai: 'TuLuan',
        })
        return await ch.save().then(c => c).catch(err => err)
    })
    .catch(err => err);
}

getMultiChoiceById = async (id) => {
    return await TracNghiem.findById(id).populate('danh_muc').then(q => q).catch(err => err)
}

getEssayById = async (id) => {
    return await TuLuan.findById(id).populate('danh_muc').then(q => q).catch(err => err)
}

getMultiChoiceByTopic = async (t_id, tp_id, title) => {
	if(tp_id.length == 0)
        return await TracNghiem.find({nguoi_tao_id: t_id, noi_dung: new RegExp(title)}).then(q => q).catch(err => err);
	return await TracNghiem.find({nguoi_tao_id: t_id, danh_muc: {$in: tp_id}, noi_dung: new RegExp(title)}).then(q => q).catch(err => err)
}

getEssayByTopic = async (t_id, tp_id, title) => {
	if(tp_id.length == 0)
		return await TuLuan.find({nguoi_tao_id: t_id, noi_dung: new RegExp(title)}).then(q => q).catch(err => err);
	return await TuLuan.find({nguoi_tao_id: t_id, danh_muc: {$in: tp_id}, noi_dung: new RegExp(title)}).then(q => q).catch(err => err)
}

updateChoiceQuestion = async (body) => {
    const {id, noi_dung, lua_chon, dap_an, diem, danh_muc} = body;
    const tn = await TracNghiem.findById(id);
    if(tn){
        tn.noi_dung = noi_dung;
        tn.lua_chon = lua_chon;
        tn.dap_an = dap_an;
        tn.diem = diem;
        tn.danh_muc = danh_muc;
        return await tn.save().then(res => res).catch(err => err);
    }else{
        return tn;
    }
}

deleteChoiceQuestion = async (id) => {
    return await TracNghiem.findByIdAndUpdate(id, {trang_thai: false}).then(tn => tn).catch(err => err);
}

deleteEssayQuestion = async (id) =>{
    return await TuLuan.findByIdAndUpdate(id, {trang_thai: false}).then(tl => tl).catch(err => err);
}


module.exports = {
    getMultiChoiceById,
    getEssayById,
	getMultiChoiceByTopic,
	getEssayByTopic,
    addChoiceQuestion,
    addEssayQuestion,
    updateChoiceQuestion,
    deleteChoiceQuestion,
    deleteEssayQuestion,
}