require('dotenv').config()
const nodemailer = require('nodemailer')

sendTo = async (to, subject, text, html) => {
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        port: 587,
        host: 'smtp.gmail.com',
        secure: false,
        requireTLS: true,
        auth: {
            user: process.env.SVEMAIL,
            pass: process.env.SVPASS,
        },
    })

    let info = {
        from: process.env.SVEMAIL,
        to: to,
        subject: subject,
        text: text,
        html: html,
    }
    transporter.sendMail(info, (err, dt) => {
        if(err) throw err;
        else{
            console.log(dt)
        }
    })
}


module.exports = {sendTo,};