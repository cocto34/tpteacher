const mongoose = require('mongoose')
const LopHoc = require('../models/lop_hoc.schema')
const SinhVien = require('../models/sinh_vien.schema')
const IndviteSV = require('./invite.service')
const inviteService = require('./invite.service')

getAllStudents = async (id) => {
    return await LopHoc.findById(id, 'ds_sinh_vien')
    .populate('ds_sinh_vien','ma_sv ho ten anh_dai_dien email')
    .then(st => st).catch(err => err)
}

getAllPosts = async (id) => {
    return await LopHoc.findById(id, 'ds_bai_viet')
    .populate('ds_bai_viet')
    .then(pt => pt).catch(err => err)
}

getAllExcers = async (id) => {
    return await LopHoc.findById(id,'ds_bai_tap -_id')
    .populate('ds_bai_tap')
    .then(ex => ex).catch(err => err)
}

getAllTests = async (id) => {
    return await LopHoc.findById(id,'ds_bai_thi -_id')
    .populate('ds_bai_thi','tieu_de nguoi_tao_id ngay_tao thoi_gian_thi')
    .then(tt => tt).catch(err => err)
}

getAllBelongToTeacher = async (t_id) => {
    return await LopHoc.find({nguoi_tao_id: t_id}).then(l => l).catch(err => err)
}

getInfoById = async (c_id) => {
    return await LopHoc.findById(c_id).then(l => l).catch(err => err)
}

addExamToClass = async (c_id, ex_id) => {
    const c = await LopHoc.findById(c_id).then(c => c).catch(err => err)
    c.ds_bai_tap.push(ex_id);
    return c.save().then(c => c).catch(err => err);
}

addTestToClass = async (c_id, ex_id) => {
    const c = await LopHoc.findById(c_id).then(c => c).catch(err => err)
    c.ds_bai_thi.push(ex_id);
    return c.save().then(c => c).catch(err => err);
}

addStudent = async (c_id, email, code) => {
    
}

removeStudent = async (c_id, s_id) => {
    const lop = await LopHoc.findById(c_id)
    const sv = await SinhVien.findById(s_id)
    const index = lop.ds_sinh_vien ? lop.ds_sinh_vien.indexOf(s_id) : -1
    const s_index = sv.ds_lop_hoc ? sv.ds_lop_hoc.indexOf(c_id) : -1
    if(index != -1 && s_index != -1){
        lop.ds_sinh_vien.splice(index, 1)
        sv.ds_lop_hoc.splice(s_index, 1)
        await sv.save().then()
    }
    else{
        return new Error('Sinh viên không tồn tại');
    }
    return await lop.save().then(c => c).catch(err => err)
}

removeExamByIndex = async (c_id, index) => {
     const c = await LopHoc.findById(c_id)
     if(c.ds_bai_tap.length >index){
     c.ds_bai_tap.splice(index,1)	
	}
	else{return new Error('Bài tập không tồn tại')}
return await c.save().then(c => c).catch(err => err);
}

removeTestByIndex = async (c_id, index) => {
    const c = await LopHoc.findById(c_id)
    if(c.ds_bai_thi.length>index){c.ds_bai_thi.splice(index,1)}
	else {return new Error('Bài thi không tồn tại')}
return await c.save().then(c=>c).catch(err => err);
}

addClassroom = async (body) =>{
    const lop = new LopHoc({
        _id: mongoose.Types.ObjectId(),
        tieu_de: body.tieu_de,
        nguoi_tao_id: body.nguoi_tao_id,
        ds_sinh_vien: body.ds_sinh_vien? body.ds_sinh_vien : [],
        ds_bai_tap: body.ds_bai_tap ? body.ds_bai_tap : [],
        ds_bai_thi: body.ds_bai_thi ? body.ds_bai_thi : [],
    })
    return await lop.save().then(l => l).catch(err => err);
}

updateClassroom = async (body) => {
    const {id, tieu_de} = body;
    const lop = await LopHoc.findById(id);
    lop.tieu_de = tieu_de;
    return await lop.save().then(l => l).catch(err => err)
}

archiveClassroom = async (id) => {
    return await LopHoc.findByIdAndUpdate(id, {trang_thai: false}).then(l => l).catch(err => err)
}

module.exports = {
    getAllStudents,
    getAllExcers,
    getAllPosts,
    getAllTests,
    getAllBelongToTeacher,
    addClassroom,
    updateClassroom,
    archiveClassroom,
    addStudent,
    removeStudent,
    getInfoById,
	addExamToClass,
	addTestToClass,
removeExamByIndex,
removeTestByIndex,
}