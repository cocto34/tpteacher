const Diem = require('../models/diem.schema')
const BaiThiSinhVien = require('../models/bai_thi_sinh_vien.schema');
const NopBaiTap = require('../models/nop_bai_tap.schema');
const mongoose = require('mongoose')

getAllByClassroom = async (id) => {
    return await Diem.find({lop_hoc_id: id})
    .then(d => d)
    .catch(err => err)
}

// getAllExByClassroom = async (id) => {
//     return await Diem.find({lop_hoc_id: id, loai: 'bai_tap'}).then(d => d).catch(err => err)
// }

getExMarkById = async (id, lop_hoc_id) => {
    return await Diem.find({ex_id: id, lop_hoc_id: lop_hoc_id, loai: 'BaiTap'})
    .populate('ex_id')
    .populate('sinh_vien_id')
    .then(d => d)
    .catch(err => err)
}

getTestMarkById = async (id, lop_hoc_id) => {
    return await Diem.find({ex_id: id, lop_hoc_id: lop_hoc_id, loai: 'BaiThi'})
    .populate('ex_id')
    .populate('sinh_vien_id')
    .then(d => d).catch(err => err)
}


mark = async (body) => {
    const {diem, sinh_vien_id, lop_hoc_id, ex_id, loai, chi_tiet_bai_lam} = body
    const mark = new Diem({
        _id: mongoose.Types.ObjectId(),
        diem: Math.round((diem+Number.EPSILON)*100)/100,
        sinh_vien_id: sinh_vien_id,
        lop_hoc_id: lop_hoc_id,
        ex_id: ex_id,
        loai: loai,
        chi_tiet_bai_lam: chi_tiet_bai_lam,
    })
    return await mark.save()
    .then(async m => {
        if(loai === 'BaiThi'){
            await BaiThiSinhVien.findOneAndUpdate({sinh_vien_id: sinh_vien_id, bai_thi_id: ex_id}, {da_cham_diem: true})
            .then(b => b).catch(err => err)
        }
        else{
            await NopBaiTap.findOneAndUpdate({sinh_vien_id: sinh_vien_id, bai_tap_id: ex_id}, {da_cham_diem: true})
            .then(b => b).catch(err => err)
        }
        return await m
    })
    .catch(err => err)
}

updateMark = async (body) => {
    const {id, diem, chi_tiet_bai_lam} = body
    return await Diem.findByIdAndUpdate(id, {diem: Math.round((diem+Number.EPSILON)*100)/100, chi_tiet_bai_lam: chi_tiet_bai_lam})
    .then(m => m)
    .catch(err => err)
}

removeMark = async (body) => {
    const {id} = body
    return await Diem.findByIdAndRemove(id)
    .then(d => d)
    .catch(err => err)
}

module.exports = {
    getAllByClassroom,
    //getAllExByClassroom,
    getExMarkById,
    getTestMarkById,
    mark,
    updateMark,
    removeMark,
}