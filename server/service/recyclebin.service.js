const BaiThi = require('../models/bai_thi.schema')
const BaiTap = require('../models/bai_tap.schema')

// getAllTests = async (t_id) => {
//     return await BaiThi.find({nguoi_tao_id: t_id, trang_thai: false}).then(bt => bt).catch(err => err)
// }

// getAllExcers = async (t_id) => {
//     return await BaiTap.find({nguoi_tao_id: t_id, trang_thai: false}).then(bt => bt).catch(err => err)
// }

forceDeleteExcer = async (id) => {
    return await BaiTap.findByIdAndDelete(id).then(b => b).catch(err => err);
}

forceDeleteTest = async (id) => {
    return await BaiThi.findByIdAndDelete(id).then(b => b).catch(err => err)
}

restoreTest = async (id) => {
    return await BaiThi.findByIdAndUpdate(id, {trang_thai: true}).then(b => b).catch(err => err)
}

restoreExcer = async (id) => {
    return await BaiTap.findByIdAndUpdate(id, {trang_thai: true}).then(b => b).catch(err => err)
}

module.exports = {
    getAllExcers,
    getAllTests,
    forceDeleteExcer,
    forceDeleteTest,
    restoreExcer,
    restoreTest,
}
