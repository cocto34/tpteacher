const NopBaiTap = require('../models/nop_bai_tap.schema')
const BaiThiSinhVien = require('../models/bai_thi_sinh_vien.schema')
const DiemService =require('./diem.service')

// getAllExInClass = async (e_id, t_id) => {
//     return await NopBaiTap.find({bai_tap_id: e_id, lop_hoc_id: t_id})
//     .then(e => e)
//     .catch(err => err)
// }

// getAllTestInClass = async (t_id) => {
//     return await BaiThiSinhVien.find({bai_thi_id: t_id})
//     .then(t => t)
//     .catch(err => err)
// }

// getExById = async (e_id) => {
//     return await NopBaiTap.findOne({bai_tap_id: e_id, trang_thai: true})
//     .then(e => e)
//     .catch(err => err)
// }

// getTestById = async (t_id) => {
//     return await BaiThiSinhVien.findOne({bai_thi_id: t_id}).then(t => t).catch(err => err)
// }

getExOfStudent = async (s_id, e_id) => {
    return await NopBaiTap.findOne({bai_tap_id: e_id, sinh_vien_id: s_id})
    .populate('bai_tap_id')
    .populate('sinh_vien_id')
    .populate('lop_hoc_id').then(ex => ex).catch(err => err)
}

getTestOfStudent = async (s_id, t_id) => {
    return await BaiThiSinhVien.findOne({bai_thi_id: t_id, sinh_vien_id: s_id})
    .populate('bai_thi_id')
    .populate('bai_thi_sinh_vien.cau_hoi_id')
    .populate('sinh_vien_id', 'ho ten')
    .then(t => t).catch(err => err)
}

addMark = async (body) => 
{
    return await DiemService.mark(body)
}

module.exports = {
    getExOfStudent,
    getTestOfStudent,
    addMark,
}