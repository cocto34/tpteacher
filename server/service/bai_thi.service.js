const mongoose = require('mongoose')
const BaiThi = require('../models/bai_thi.schema')

getAll = async ()=>{
    
}

getAllBelongToTeacher = async (t_id) =>{
    return await BaiThi.find({nguoi_tao_id: t_id, trang_thai: true})
    .populate('nguoi_tao_id')
    .populate('lop_hoc_id')
    .populate('ds_cau_hoi.cau_hoi_id')
    .sort({createdAt: -1})
    .then(bt => bt)
    .catch(err => err)
}

getTestInClassroom = async (c_id) => {
    return await BaiThi.find({lop_hoc_id: c_id, trang_thai: true})
    .populate('nguoi_tao_id')
    .populate('lop_hoc_id')
    .populate('ds_cau_hoi.cau_hoi_id')
    .sort({createdAt: -1})
    .then(bt => bt)
    .catch(err => err)
}

getById = async (id) => {
    return await BaiThi.findById(id)
    .populate('nguoi_tao_id')
    .populate('lop_hoc_id')
    .populate('ds_cau_hoi.cau_hoi_id')
    .then(bt => bt)
    .catch(err => err)
}

addFifteenMin = async (ngay_thi) => {
    var time = new Date(ngay_thi).getTime() + 1000*60*15;
    return new Date(time);
}

addTest = async (body) => {
    const {tieu_de, nguoi_tao_id, lop_hoc_id, ngay_thi, thoi_gian_thi, ds_sinh_vien, ds_cau_hoi} = body;
    const bt = new BaiThi({
        _id: mongoose.Types.ObjectId(),
        tieu_de: tieu_de,
        nguoi_tao_id: nguoi_tao_id,
        lop_hoc_id: lop_hoc_id,
        ngay_thi: ngay_thi,
        thoi_gian_thi: thoi_gian_thi,
        ds_sinh_vien: ds_sinh_vien ? ds_sinh_vien : [],
        ds_sinh_vien_da_thi: [],
        thoi_gian_tre: await addFifteenMin(ngay_thi),
	    ds_cau_hoi: ds_cau_hoi ? ds_cau_hoi : [],
    })
    return await bt.save().then(b => b).catch(err => err);
}

addTestComment = async (t_id, cmt) => {
    const bt = await BaiThi.findById(t_id)
    bt.ds_binh_luan.push(cmt)
    return await bt.save().then(t => t).catch(err => err)
}

updateTest = async (body) => {
    const {id, tieu_de, ngay_thi, thoi_gian_thi, ds_cau_hoi} = body
    return await BaiThi.findByIdAndUpdate(id, {
        tieu_de: tieu_de,
        ngay_thi: ngay_thi,
        thoi_gian_thi: thoi_gian_thi,
        //ds_sinh_vien: ds_sinh_vien,
        thoi_gian_tre: await addFifteenMin(ngay_thi),
	    ds_cau_hoi: ds_cau_hoi,
    }).then(bt => bt).catch(err => err)
}

deleteTest = async (id) => {
    return await BaiThi.findByIdAndUpdate(id, {trang_thai: false}).then(bt => bt).catch(err => err)
}

removeTestComment = async (u_id, c_id) => {
    const bt = await BaiThi.findById({nguoi_tao_id: u_id})
    const index = bt.ds_binh_luan.findIndex(x => x.id == c_id)
    bt.ds_binh_luan.splice(index,1)
    return await bt.save().then(b => b).catch(err => err)
}

module.exports = {
    getAllBelongToTeacher,
    getTestInClassroom,
    getById,
    addTest,
    updateTest,
    deleteTest,
    addTestComment,
    removeTestComment,
}