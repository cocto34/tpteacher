
const DanhMuc = require('../models/danh_muc.schema')

getTopicById = async (id) => {
    return await DanhMuc.findOne({_id: id, trang_thai: true}).then(dm => dm).catch(err => err);
}

getAll = async () => {
    return await DanhMuc.find({trang_thai: true}).then(d => d).catch(err => err);
}

module.exports = {
    getTopicById,
    getAll,
}
