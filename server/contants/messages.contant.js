MESSAGES = {
    USER_NOT_EXIST : 'Người dùng không tồn tại.',
    PASSWORD_WRONG : 'Mật khẩu không đúng.',
    THROW_ERROR: 'Lỗi rồi',
};
isProduction = true
FE_URL_TMP = 'http://localhost:3000/au/active/forgotpassword'
FE_URL = 'http://navilearntest.herokuapp.com/au/active/forgotpassword'
host = isProduction ? FE_URL : FE_URL_TMP
/**
 * @param {} c_name
 * @param {} t_name
 * @param {} code
 */

InviteHTML = (c_name, t_name, code) => {
    return `
    <h3>Lớp học ${c_name} của giáo viên ${t_name} muốn mời bạn tham gia lớp học</h3></br>Hãy nhập mã tham gia là: ${code}`
}

ResetPasswordHTML = (code) => {
    return `
    <h3>Chào bạn</h3></br>Hãy nhấn vào liên kết này để lấy lại mật khẩu: 
    <a href="${host}/${code}">
    ${host}/${code}</a><p style='color:red'>Nếu như bạn không phải là người gửi yêu cầu thì hãy bỏ qua email này.</p>
    `
}

module.exports = {
    MESSAGES,
    InviteHTML,
    ResetPasswordHTML,
}